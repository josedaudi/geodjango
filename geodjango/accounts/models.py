from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User


class AppCustomers(models.Model):
    names = models.CharField(max_length=50)
    user = models.ForeignKey(User, null=True)
    location = models.PointField(srid=4326)
    # object = models.GeoManager()

    def __unicode__(self):
        return self.user

    class Meta:
        db_table = 'app_customers'
        verbose_name_plural = 'Customers'


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    role = models.CharField(max_length=20)

    def __unicode__(self):
        return self.names

    class Meta:
        db_table = 'app_user_profile'
        verbose_name_plural = 'Users'


class Regions(models.Model):
    region_cod = models.CharField(max_length=50)
    region_nam = models.CharField(max_length=50)
    geom = models.MultiPolygonField(srid=4326)

    def __unicode__(self):
        return self.region_nam

    class Meta:
        verbose_name_plural = 'Regions'


class StoreItems(models.Model):
    item_name = models.CharField(max_length=50)
    item_category = models.CharField(max_length=20)
    item_unit = models.CharField(max_length=10)
    item_price = models.CharField(max_length=20)

    def __unicode__(self):
        return self.item_name

    class Meta:
        db_table = 'app_store'
        verbose_name_plural = 'Items'


class SaleItem(models.Model):
    customer = models.ForeignKey(AppCustomers)
    item = models.ForeignKey(StoreItems)
    price = models.CharField(max_length=20)
    attendee = models.ForeignKey(User)
    transaction_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.transaction_date

    class Meta:
        db_table = 'app_store_sales'
        verbose_name_plural = 'Sales'


