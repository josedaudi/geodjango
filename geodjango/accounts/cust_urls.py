from accounts.views import create_customer, edit_customer, delete_customer
from django.conf.urls import url


urlpatterns = [
    url(r'^create/$', create_customer, name='create'),
    url(r'^edit/$', edit_customer, name='edit'),
    url(r'^delete/$', delete_customer, name='delete')
]
