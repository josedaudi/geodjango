from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.serializers import serialize
from django.contrib.auth import authenticate, logout, login
from django.template.context_processors import csrf
from rest_framework.views import APIView
from .forms import UserLoginForm
from .models import Regions, AppCustomers, SaleItem, UserProfile


def init(request):
    user = request.user
    if user.is_authenticated:
        pro = UserProfile.objects.get(user=request.user)
        return render_to_response('dashboard.html', {'user': user, 'profile': pro})
    else:
        return render_to_response('dashboard.html', {'user': user})


def regions(request):
    reg = serialize('geojson', Regions.objects.all())
    return HttpResponse(reg, content_type='json')


def sales(request):
    store = serialize('geojson', SaleItem.objects.select_related())
    return HttpResponse(store, content_type='json')


def customer(request):
    cus = serialize('json', AppCustomers.objects.get(user=request.user))
    return HttpResponse(cus, content_type='json')


def customers(request):
    cus = serialize('geojson', AppCustomers.objects.all())
    return HttpResponse(cus, content_type='json')


def dashboard(request):
    user = request.user
    if user.is_authenticated:
        context = {}
        context.update(csrf(request))
        pro = UserProfile.objects.get(user=user)
        return render_to_response('dashboard.html', {'profile': pro, 'user': user, })
    else:
        return HttpResponseRedirect('/accounts/login/')

# class RegionsView(APIView):
#     def regions(request):
#         reg = serialize('geojson', Regions.objects.all())
#         return HttpResponse(reg, content_type='json')
#
#
# class CustomersView(APIView):
#     model = AppCustomers
#
#     def customers(self):
#         cus = serialize('geojson', AppCustomers.objects.all())
#         return HttpResponse(cus, content_type='json')


def sign_in(request):
    context = {}
    context.update(csrf(request))
    return render_to_response('index.html', context)


def login_auth(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    return HttpResponseRedirect('/accounts/login/')
            else:
                return HttpResponseRedirect('/accounts/login/')
        else:
            return HttpResponseRedirect('/accounts/login/')
    else:
        return HttpResponseRedirect('/accounts/login/')


def profile(request):
    user = request.user
    if user.is_authenticated:
        pro = UserProfile.objects.get(user=user)
        return render_to_response('profile.html', {'user': user, 'profile': pro})
    else:
        return HttpResponseRedirect('/accounts/login/')


def create_customer(request):
    user = request.user
    if user.is_authenticated:
        context = {}
        context.update(csrf(request))
        reg = Regions.objects.all()
        return render_to_response('create_customer.html', {'user': user, 'context': context, 'regions': reg})
    else:
        return HttpResponseRedirect('/accounts/login/')


def delete_customer(request):
    user = request.user
    if user.is_authenticated:
        context = {}
        context.update(csrf(request))
        return render_to_response('create_customer.html', context)
    else:
        return HttpResponseRedirect('/accounts/login/')


def edit_customer(request, cid):
    user = request.user
    if user.is_authenticated:
        context = {}
        context.update(csrf(request))
        c = AppCustomers.objects.get(id=cid)
        return render_to_response('create_customer.html', context, {'data': c})
    else:
        return HttpResponseRedirect('/accounts/login/')


def sign_out(request):
    logout(request)
    return HttpResponseRedirect('/')

