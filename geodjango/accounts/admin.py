from django.contrib import admin
from accounts.models import AppCustomers, Regions
from leaflet.admin import LeafletGeoAdmin
from .models import SaleItem, StoreItems, UserProfile


class StoreItemAdmin(admin.ModelAdmin):
    list_display = ('item_name', 'item_unit', 'item_category', 'item_price')


class ItemSalesAdmin(admin.ModelAdmin):
    list_display = ('transaction_date', 'item', 'attendee')


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'role')


class AppCustomersAdmin(LeafletGeoAdmin):
    list_display = ('names', 'location')


class RegionsAdmin(LeafletGeoAdmin):
    list_display = ('region_nam', 'region_cod')
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(StoreItems, StoreItemAdmin)
admin.site.register(SaleItem, ItemSalesAdmin)
admin.site.register(AppCustomers, AppCustomersAdmin)
admin.site.register(Regions, RegionsAdmin)
