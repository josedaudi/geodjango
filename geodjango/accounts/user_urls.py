from accounts.views import sign_out, sign_in, profile, dashboard, login_auth
from django.conf.urls import url


urlpatterns = [
    url(r'^profile/$', profile, name='profile'),
    url(r'^dashboard/$', dashboard, name='dashboard'),
    url(r'^signin/$', login_auth, name='signin'),
    url(r'^login/$', sign_in, name='login'),
    url(r'^logout/$', sign_out, name='logout')
]
