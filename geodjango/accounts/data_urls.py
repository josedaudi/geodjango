from accounts.views import regions, customers, sales, customer
from django.conf.urls import url


urlpatterns = [
    url(r'^regions/$', regions, name='regions'),
    url(r'^customers/$', customers, name='customers'),
    url(r'^customer/$', customer, name='customer'),
    url(r'^sales/$', sales, name='sales')
]
