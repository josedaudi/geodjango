from accounts.views import init
from django.conf.urls import url


urlpatterns = [
    url(r'^', init, name='home'),
]
