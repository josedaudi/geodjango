import os
from django.contrib.gis.utils import LayerMapping
from accounts.models import Regions


tanzania_mapping = {
    'region_cod' : 'Region_Cod',
    'region_nam' : 'Region_Nam',
    'geom' : 'MULTIPOLYGON',
}

tz_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data/Regions.shp'))


def execute(verbose=True):
    tz_lm = LayerMapping(Regions, tz_shp, tanzania_mapping, transform=False, encoding='iso-8859-1')
    tz_lm.save(strict=True, verbose=verbose)
